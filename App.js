/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
      
      
      <View style={styles.content}>
      <View style={[styles.circle, styles.center]}>
          <Text style={styles.headerText}>image</Text>
        </View>
      </View>
        

        <View style={styles.content}>
          <View style={styles.row}>
            <Text>Text</Text>
          </View>
          <View style={styles.row}>
            <Text>Text</Text>
          </View>
          <View style={styles.box1}>
           
              <Text style={styles.textC} > touch</Text>
             
          </View>

        </View>
      </View>

      




    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#595d60',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey'
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    margin: 15,
    alignItems: 'center',
    justifyContent: 'space-around',
    
  },
  content: {
    backgroundColor: '#595d60',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

  },
  box1: {
    backgroundColor: 'black',
    margin: 50,
    width: 400,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
   
  },
  textC:
  {
    color: 'white',
    justifyContent: 'center',
  },
  box2: {
    backgroundColor: '#755f76',
    flex: 1,
    margin: 20,
    alignItems: 'center',

  },
  row: {
    backgroundColor: 'white',
    margin: 5,
    flex : 1,
    flexDirection: 'row',
    width: 400,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  circle: {
    
    width: 200,
    height: 200,
    borderRadius: 200 / 2,
    color :'black',
    backgroundColor: 'grey',
    alignItems: 'center',
  },
  center: {
    justifyContent: 'center'
  }
});