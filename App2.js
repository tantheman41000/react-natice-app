/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App2 extends Component<Props> {
    render() {
        return (
            <View style = {{flex:1}}>
                <View style={[styles.header,]}>
                    <Text style={[styles.TextColor, styles.sessionSide]}>=</Text>
                    <Text style={[styles.TextColor, styles.sessionmiddle]}>Text</Text>
                    <Text style={[styles.TextColor, styles.sessionSide]}>X</Text>
                </View>

                <View style={styles.contend}>
                    <Text >Text</Text>
                </View>

                <View style={[styles.header,]}>
                    <Text style={[styles.TextColor, styles.sessionIcon]}>I</Text>
                    <Text style={[styles.TextColor, styles.sessionIcon]}>C</Text>
                    <Text style={[styles.TextColor, styles.sessionIcon]}>O</Text>
                    <Text style={[styles.TextColor, styles.sessionIcon]}>N</Text>
                </View>


            </View>






        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    TextColor: {
        color: 'white',
    },
    sessionmiddle: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '80%',
        height: 50,
    }
    ,
    sessionSide: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '10%',
        height: 50,
       
    },
    sessionIcon: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '25%',
        height: 50,
        
    },
    contend: {
        // width: '100%',
        // height: '85%',
        backgroundColor: 'blue',
        flex:1
    }
});